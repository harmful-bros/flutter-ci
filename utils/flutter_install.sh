#!/bin/bash

PWD=`pwd`

CHROME_WEB="http://dl.google.com/linux/chrome/deb/"
APT_LIST="/etc/apt/sources.list.d/google.list"

BIN_DIR="/usr/bin"
CHROME_BIN="${BIN_DIR}/google-chrome"
JAVA_BIN="${BIN_DIR}/java"
VSC_BIN="${BIN_DIR}/code"

ANDROID_DIR="/opt/android-studio"
ANDROID_SDK="${PWD}/android_sdk"
CMDTOOLS="commandlinetools-linux-7583922_latest.zip"

check_root()
{
	SUDO=""
	if [[ $(id -u) -ne 0 ]]
	then
		SUDO="sudo"
	fi

	echo "Completed to get the ROOT permission"
}

install_prerequisite()
{
	${SUDO} apt update
	${SUDO} apt install -y software-properties-common \
		curl \
		git \
		xz-utils \
		libglu1-mesa \
		unzip \
		wget \
		gnupg2 \
		clang \
		cmake \
		ninja-build \
		pkg-config \
		libgtk-3-dev

	echo "Completed to install the prerequisite"
}

install_flutter()
{
	export PATH="${PATH}:${PWD}/flutter/bin"

	if [ -d "flutter" ]
	then
		echo "Already installed the flutter"
		return
	fi

	git clone https://github.com/flutter/flutter.git -b stable

	echo "Completed to install the flutter"
}

install_chrome()
{
	if [ -f "${CHROME_BIN}" ]
	then
		echo "Already installed the google-chrome"
		return
	fi

	# get the key for updating
	wget \
		-q \
		-O \
		- https://dl-ssl.google.com/linux/linux_signing_key.pub | \
		${SUDO} apt-key add -

	# add the apt list for the install
	${SUDO} sh -c \
		"echo \"deb [arch=amd64] ${CHROME_WEB} stable main\" >> ${APT_LIST}"

	# install the chrome
	${SUDO} apt update
	${SUDO} apt install -y google-chrome-stable

	# remove the google.list
	${SUDO} rm -rf /etc/apt/sources.list.d/google.list

	echo "Completed to install the Google Chrome"
}

install_android_studio()
{
	if [ ! -f "${JAVA_BIN}" ]
	then
		# install the openjdk
		${SUDO} apt install -y openjdk-11-jdk
	fi

	if [ ! -d "${ANDROID_DIR}" ]
	then
		# install the android studio
		echo | ${SUDO} add-apt-repository ppa:maarten-fonville/android-studio
		${SUDO} apt update
		${SUDO} apt install -y android-studio
	fi

	# enable the linux settings for the flutter
	flutter config --enable-linux-desktop

	echo "Completed to install the Android Studio"
}

install_android_toolchain()
{
	if [ -d "${ANDROID_SDK}" ]
	then
		echo "Already installed the android sdk"
		return
	fi

	# install cmdline-tools
	mkdir -p ${ANDROID_SDK}/cmdline-tools/latest
	wget https://dl.google.com/android/repository/${CMDTOOLS}
	mkdir -p temp
	unzip ${CMDTOOLS} -d temp
	cp -rf temp/cmdline-tools/* ${ANDROID_SDK}/cmdline-tools/latest
	rm -rf ${CMDTOOLS} temp

	# install the android toolchain
	yes | ${ANDROID_SDK}/cmdline-tools/latest/bin/sdkmanager --install \
		"platform-tools" \
		"platforms;android-29" \
		"build-tools;29.0.2"

	# set the path to android sdk
	flutter config --android-sdk ${ANDROID_SDK}

	# add the android licenses
	yes | flutter doctor --android-licenses

	echo "Completed to install the Android toolchain"
}

install_visual_studio_code()
{
	if [ -f "${VSC_BIN}" ]
	then
		echo "Already installed the Visual Studio Code"
		return
	fi

	# get the key for updating
	wget \
		-qO- https://packages.microsoft.com/keys/microsoft.asc \
		| gpg --dearmor > packages.microsoft.gpg

	# add the apt list for the install
	${SUDO} install \
		-o root \
		-g root \
		-m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/

	${SUDO} sh -c \
		'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'

	rm -f packages.microsoft.gpg

	# install the VSCode
	${SUDO} apt install apt-transport-https
	${SUDO} apt update
	${SUDO} apt install -y code

	# add the plugins
	code --install-extension aksharpatel47.vscode-flutter-helper
	code --install-extension alexisvt.flutter-snippets
	code --install-extension AzMoza.aqueduct-helper
	code --install-extension Dart-Code.dart-code
	code --install-extension Dart-Code.flutter
	code --install-extension esskar.vscode-flutter-i18n-json
	code --install-extension everettjf.pubspec-dependency-search
	code --install-extension FelixAngelov.bloc
	code --install-extension gmlewis-vscode.flutter-stylizer
	code --install-extension gornivv.vscode-flutter-files
	code --install-extension luanpotter.dart-import
	code --install-extension oscarcs.dart-syntax-highlighting-only
	code --install-extension pflannery.vscode-versionlens

	echo "Completed to install the Visual Studio Code"
}

###############################################################################

set -e

check_root
install_prerequisite
install_flutter

CHECKER=$(flutter doctor | grep "Doctor found issues")
if [[ ${CHECKER} != "" ]]
then
	install_chrome
	install_android_studio
	install_android_toolchain
	install_visual_studio_code
fi

flutter doctor -v

echo "Done!"
