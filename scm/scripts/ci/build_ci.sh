#!/bin/bash

ROOT_DIR=$(git rev-parse --show-toplevel)
COMMON_DIR=${ROOT_DIR}/scm/scripts/common
OUTPUT_DIR="build/app/outputs/bundle/release"

set -e

if [[ "$1" == "" ]]
then
	echo_func "[scm] Need to flutter project directory" 1
	exit 1
fi

source ${COMMON_DIR}/echo.sh

cd $1
flutter build -v appbundle -v
if [ ! -f "${OUTPUT_DIR}/app-release.aab" ]
then
	echo_func "[scm] Failed to build for th appbundle!" 1
	exit 1
fi

echo_func "[scm] Flutter CI build done!" 0
