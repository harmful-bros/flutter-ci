#!/bin/bash

ROOT_DIR=$(git rev-parse --show-toplevel)
COMMON_DIR=${ROOT_DIR}/scm/scripts/common

set -e

if [[ "$1" == "" ]]
then
	echo_func "[scm] Need to flutter project directory" 1
	exit 1
fi

source ${COMMON_DIR}/echo.sh

cd $1

# execute the unittest
find \
	${ROOT_DIR} \
	-not -path '*/main*' \
	-name "*.dart" \
	-exec flutter test -vv --suppress-analytics {} \;

echo_func "[scm] Flutter CI unittest done!" 0
