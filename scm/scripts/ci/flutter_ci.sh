#!/bin/bash

ROOT_DIR=$(git rev-parse --show-toplevel)
COMMON_DIR=${ROOT_DIR}/scm/scripts/common
YAML_FILE="analysis_options.yaml"

set -e

if [[ "$1" == "" ]]
then
	echo_func "[scm] Need to flutter project directory" 1
	exit 1
fi

source ${COMMON_DIR}/echo.sh

cd $1
if [ ! -f ${YAML_FILE} ]
then
	echo_func "[scm] yaml not exist." 1
	exit 1
fi

flutter pub get -vv
flutter analyze -vv

echo_func "[scm] Flutter CI test done!" 0
