# For Flutter-ci


## Docker Installation (x86 only)


### Set Up the Repository


```bash
$ sudo apt update
$ sudo apt install apt-transport-https ca-certificates curl gnupg lsb-release
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
$  echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```


### Install Docker Engine


```bash
$ sudo apt update
$ sudo apt install docker-ce docker-ce-cli containerd.io
```


* Refer to the link below

  * [Install the Docker on Ubuntu](https://docs.docker.com/engine/install/ubuntu/)


## Add the User to the Docker Group


```bash
$ sudo usermod -aG docker $USER
$ sudo reboot
```

## Check the Docker Commands


```bash
$ docker images
```
